var concat = require("grunt-contrib-concat");
var activeModules = require("./core/modules/modules.json");
var ftpconfig = require('./.ftpconfig');

// Get include lists for modules
var modules = [];
modules.sass = [];
modules.scripts = [];
modules.views = [];
for (var i=0; i < activeModules.active.length; i++) {
    modules.sass.push("core/modules/" + activeModules.active[i] + "/scss/**.scss");
    modules.scripts.push("core/modules/" + activeModules.active[i] + "/js/**.js");
    modules.views.push("core/modules/" + activeModules.active[i] + "/views/**.hbs");
}

function combineSassSources(base, modules){
    return base.concat(modules);
}

module.exports = {
    sass: {
        src: combineSassSources(["core/scss/**/*.scss"], modules.sass),
        dest: "./stage/css/"
    },
    scripts: {
        src: modules.scripts,
        dest: "./stage/scripts",
        fileName: "responsive-base-modules.js"
    },
    upload: {
        dest: ftpconfig.ftpPath,
        base: ".",
        globs: [
            'stage/css/**',
            'stage/scripts/**',
            'stage/images/responsive-base/**',
            '!stage/css/support.css'
        ]
    },
    views: {
        src: modules.views,
        dest: "stage/scripts/",
        nameSpace: "J.views",
        fileName: "responsive-base-views.js"
    },
    watch: {
        upload: [
            'stage/css/**/**',
            'stage/scripts/**/**',
            'stage/images/responsive-base/**/**'

        ]
    }
};
