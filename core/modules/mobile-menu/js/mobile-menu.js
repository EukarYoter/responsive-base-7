var mobileMenu = {
    wrapper: "#menu-content",
    menuOverlay: "#menu-overlay",
    searchElement: "#search-box-wrapper",
    cartElement: ".cart-area-wrapper",
    menuActivator: "#menu-activator",
    searchActivator: "#search-activator",
    cartActivator: "#cart-activator",
    init: function () {
        J.translations.push(
            {
                search: {
                    sv: "Sök",
                    en: "Search",
                    da: "Søg",
                    nb: "Søke",
                    fi: "Haku"
                }
            },
            {
                menu: {
                    sv: "Meny",
                    en: "Menu",
                    da: "Menu",
                    nb: "Meny",
                    fi: "Valikko"
                }
            }
        );
        // DOM Manipulation
        if (J.data.jetshopData && J.checker.isLoggedIn) {
            $("a.mypages-text").removeClass("button").appendTo(".page-list-wrapper");
        } else {
            $("a.mypages-text").hide();
        }
        $("#header a.login-text").removeClass("button").appendTo(".page-list-wrapper");
        $(".small-cart-body").prepend("<span class='title'>" + J.translate("cart") + "</span>");
        // Bind functions
        $(mobileMenu.cartActivator + " span").html(J.translate("cart"));
        $(mobileMenu.searchActivator + " span").html(J.translate("search"));
        $(mobileMenu.menuActivator + " span").html(J.translate("menu"));
        $(mobileMenu.menuActivator).add(mobileMenu.menuOverlay).on("click", function () {
            if (!$("html.menu-open").length) {
                $("html").removeClass("cart-open").removeClass("search-open");
                if (typeof catNavMobile.checkMobileMenuLayout !== "undefined") {
                    catNavMobile.checkMobileMenuLayout(true);
                }
            }
            setTimeout(function () {
                $("html").toggleClass("menu-open");
            }, 30);
        });
        $(mobileMenu.searchActivator).on("click", function () {
            $("html").toggleClass("search-open");
            mobileMenu.measureWidth();
            if ($("html.search-open").length) {
                $("html").removeClass("cart-open").removeClass("menu-open");
                $(".search-box-input > input").focus();
            }
        });
        $(mobileMenu.cartActivator).on("click", function () {
            $("html").toggleClass("cart-open");
            if ($("html.cart-open").length) {
                $("html").removeClass("menu-open").removeClass("search-open");
            }
        });
        J.switch.addToLargeUp(mobileMenu.closeAll);
    },
    closeAll: function () {
        $("html").removeClass("menu-open").removeClass("search-open").removeClass("cart-open");
    },
    measureWidth: function () {
        if (J.deviceWidth === "small" || J.deviceWidth === "medium") {
            if ($("html.search-open").length) {
                var menuWrapperWidth = ($("#menu-content").outerWidth(true));
                var menuIconWidth = $("#menu-activator").outerWidth(true);
                var cartIconWidth = $(".cart-area-wrapper").outerWidth(true);
                var searchIconWidth = $("#search-activator").outerWidth(true);
                var searchWrapperWidth = menuWrapperWidth - menuIconWidth - cartIconWidth - 1;
                var searchInputWidth = searchWrapperWidth - searchIconWidth - 16; //TODO Why these numbers?
                $("#search-box-wrapper").width(searchWrapperWidth);
                $(".search-box-wrapper").width(searchInputWidth);
            } else {
                $("#search-box-wrapper").width("auto");
                $(".search-box-wrapper").width(0);
            }
        } else {
            $("#search-box-wrapper").width("auto");
            $(".search-box-wrapper").width("auto");
        }
    }
};

J.pages.addToQueue("all-pages", mobileMenu.init);

$(window).resize(function () {
    mobileMenu.measureWidth();
});
