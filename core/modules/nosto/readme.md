# Nosto recommendations

Login to Nosto admin: [my.nosto.com/admin](https://my.nosto.com/admin)

Nosto Javascript documentation: [support.nosto.com](https://support.nosto.com/manuals/using-javascript-recommendation-templates/?nosto=singlearticle-nosto-history)

All Nosto templates are programmed using Apache Velocity: [velocity.apache.org](http://velocity.apache.org/) 


### Activation

Contact Jetshop sales for walkthrough and pricing information. Jetshop will contact Nosto and start the setup.
 
Go to Nosto signup and create and account: [my.nosto.com/auth/signup](https://my.nosto.com/auth/signup) 

In Jetshop's system Nosto is activated by Jetshop Support, contact them and provide your account-ID.


### Requirements

Nosto requires the Responsive Base Framework.

The module `nosto` should be enabled in `core/modules/modules.json`, and files published to server using Grunt.


### Nosto Templates

Templates for Jetshop in Nosto should be automagically setup by Nosto, and available in Nosto's admin from start.

All templates will inherit settings from `base-settings.scss` and very little configuration is needed.

Template `jetshop-product-list-standard` are the recommended default template. 

Template `jetshop-product-list-vertical` are for vertical layout, used in the Checkout.

Template `jetshop-product-list-JSON` will output JSON, and are a goodf fit for use with the Handlebar-view present in module. Not tested.


### Nosto Template properties

The Jetshop-templates will respect most of the properties per recommendation available in Nosto's admin, however some properties are inherited from the Responsive Base-framework, for example amount of products per row will inherit from `base-settings.scss` 

![Nosto Properties](https://bytebucket.org/jetshopdesign/responsive-base/wiki/images/nosto/properties.png)


**Supported properties in Nosto Admin:**

- Show the heading above products
- Show product images
- Show product name
- Show product price
- Show the original price of discounted items
- Show product description

Do not use CSS or JS to hide parts in templates, use the properties in Nosto Admin.

Property-section "Styles" are ignored, those settings are inherited from `base-settings.scss`


### Javascript in templates

Access and messaging between the template and regular JS are a bit weird, it's highly recommended to read the documentation:

Nosto Javascript documentation: [support.nosto.com](https://support.nosto.com/manuals/using-javascript-recommendation-templates/?nosto=singlearticle-nosto-history)

**In short:**

All JS inside of templates need to be prefixed with `_targetWindow` to access the document scope.

**Example:**

`_targetWindow.$('.product-name').matchHeight(_targetWindow.J.config.sameHeightConfig);`



### Module 'nosto' in Responsive Base

The module are exposed as `window.Nosto`

This fairly simple class will inject the Nosto placeholderdivs into DOM, and provides some helpers.

JS-event `nosto-complete` will be triggered in the `window` scope after each Nosto recommendation tag are rendered.

To modify the output and placement of Nosto divs adjust corresponding object in `Nosto.pages`

Example for activating recommendations onstart page:  `J.pages.addToQueue("start-page", Nosto.pages.start)`


#### Module 'nosto' settings

`showOnlyOneRow` - Only display one row of producs. Will detect multiple rows and hide products.   

(Helper `Nosto.helpers.hideRows` will trigger on JS-event `nosto-complete` if `showOnlyOneRow` are set to `true`) 
